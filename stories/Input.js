import React from 'react';
import RaisedButton from 'material-ui/RaisedButton';

const InputStyles = {
  border: '1px solid #eee',
  borderRadius: 3,
  backgroundColor: '#FFFFFF',
  cursor: 'pointer',
  fontSize: 15,
  padding: '3px 10px',
  margin: 10,
};

const Input = ({ children, text }) => (
  <div>
  <input type="text"
    style={InputStyles}
    value={text}
  />
  </div>
);

Input.propTypes = {
  children: React.PropTypes.string.isRequired,
  onClick: React.PropTypes.func,
};

export default Input;
