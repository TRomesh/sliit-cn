import React from 'react';
import TextField from 'material-ui/TextField';
import RaisedButton from 'material-ui/RaisedButton';
import {Card, CardActions, CardHeader, CardMedia, CardTitle, CardText} from 'material-ui/Card';
import FlatButton from 'material-ui/FlatButton';
import Colors from 'material-ui/styles/colors';

var imgUrl = 'http://sp2.cinedor.es/728/foto-andrew-garfield-y-emma-stone-en-the-amazing-spider-man-3-781.jpg';

const loginStyle = {
   maxWidth: '100%',
   maxHeight: '100%',
   backgroundRepeat:'no-repeat',
   backgroundSize:'cover'

};

const buttonStyle = {
  marginTop: 25
}

const textStyle = {
  width: 300,
  marginRight: 25
};

const homeStyle = {
    marginTop: '45',
    height: '100%'
}

const loginRowStyle = {
    marginBottom: '45',
    height: '100%'
}

const logoStyle = {
    color: 'white',
    fontSize: '50'
}

const error = {
  color: 'red'
};

const signupStyle = {
    color: 'black',
    marginTop: '25'
}

const images = ['url(/img/slide1.jpg)', 'url(/img/slide2.jpg)', 'url(/img/slide3.jpg)'];

function validateEmail(email) {
  let re = /\S+@\S+\.\S+/;
  if(re.test(email)) {
    return true;
  }
  else {
    return false;
  }
}

function validatePassword(password) {
  if(password.length < 6) {
    return false;
  }
  let re = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)/;
  if(re.test(password)) {
    return true;
  }
}

const Login = React.createClass({
  getInitialState: function() {
    let current = 0;
    return {
      apitoken: '',
      err: ''
    };
  },
  componentDidMount: function() {
  
  },
  componentWillUnmount: function() {
    document.body.style.background = '';
  },
  _onChange: function() {
    
  },
  _handleLogin: function() {
    let username = this.refs.password.input.value;
    let password = this.refs.password.input.value;
    if(username === "" || password === "") {
    	this.setState({
    		err: 'please provide credentials' 
    	});
    	return;
    }
    else {
    	fetch('/api/v1/login', {
			  method: 'POST',
			  headers: {
			    'Accept': 'application/json',
			    'Content-Type': 'application/json'
			  }
			}).then(function(response) {
				return response.json();
			})
			.then(function(res) {
				if(res.done) {
					localStorage.setItem('token', res.token);
					window.location = '/itemlist';
				}
			});
    }
  },
  render: function() {
    return (
      <div style={loginStyle}>
        <div className="container-fluid" style={homeStyle}>
          <div className="row-fluid" style={loginRowStyle}>
            <div className="col-md-12 col-lg-12">
              <span id="logo-login" style={logoStyle}> Coupley </span>
            </div>
          </div>
          <div className="row-fluid">
            <div className="col-sm-6 col-md-6 col-md-offset-2 col-lg-4 col-lg-offset-4">
              <Card>
                <CardTitle title="Login" />
                <CardText>
                  <TextField
                  floatingLabelText="username" ref="username" 
                  errorText={this.state.emailerr} fullwidth={true}/>
                  
                <TextField
                  floatingLabelText="password" type="password" ref="password" 
                  errorText={this.state.pwderr} fullwidth={true}/>
                  <span id="passwordval" style={error}> </span>
                </CardText>
                <CardActions>
                  <RaisedButton label="Signin" style={buttonStyle} primary={true} onTouchTap={this._handleLogin} />
                  <a href="/#/forgotpwd"> Forgot password ? </a>
                </CardActions>
                <span id="server-error" style={error}> {this.state.err} </span>
              </Card> 

              <Card style={signupStyle}>
                <CardText>
                  Don't have an account ?
                  <a href="/#/register"> Sign up </a> for free ! 
                </CardText>
              </Card> 
            </div>
          </div>
        </div>
      </div>
    );
  }
});

export default Login;