// Simple async test for HTTP 200 response code using supertest
'use strict';

var request = require("supertest"),
    app = require("../server/app").getApp;

describe('GET /', function(){
  it('expects HTTP response 200', function(done){
    request(app)
     .get('/')
	 .expect(200, done); 
  });
  it('expects HTTP response 200', function(done){
    request(app)
     .get('/api/v1/login')
	 .expect(200, done); 
  });
  it('expects HTTP response 200', function(done){
    request(app)
     .get('/api/v1/register')
	 .expect(200, done); 
  });
  it('expects HTTP response 200', function(done){
    request(app)
     .get('/api/v1/cashier/pending')
	 .expect(200, done); 
  });
});