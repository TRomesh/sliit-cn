const jwt = require('jsonwebtoken');
const cashierModel = require('../models/cashier');

const cashierFacade = (function() {
	const verify = function() {
		return "done";
	}

	const pending = function(username, password) {
		return new Promise(function(resolve, reject) {
			cashierModel.validateLogin(function(error, docs) {
				if(error) reject('error !');

				resolve(docs);
			});
		});
	}

	return {
		pending: pending,
		done: done
	};
})();

module.exports = cashierFacade;
