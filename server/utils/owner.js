const jwt = require('jsonwebtoken');
const ownerModel = require('../models/owner');

const ownerFacade = (function() {
	const verify = function() {
		return "done";
	}

	const pending = function(username, password) {
		return new Promise(function(resolve, reject) {
			ownerModel.validateLogin(function(error, docs) {
				if(error) reject('error !');

				resolve(docs);
			});
		});
	}

	return {
		pending: pending,
		done: done
	};
})();

module.exports = ownerFacade;
