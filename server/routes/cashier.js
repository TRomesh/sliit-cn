const express = require('express');
const jwt = require('jsonwebtoken');
const auth = require('../utils/auth');
const cashierProc = require('../utils/cashier');

const cashierRoutes = function(router, bodyParser, mongoose) {
	router.get('/api/v1/cashier/pending', bodyParser.json(), function (req, res) {
		cashierProc.pending()
			.then(function(result) {
				res.json(result);
			})
			.catch(function(error) {
				res.json(error);
			});
	})

	.post('/api/v1/cashier/done', bodyParser.json(), function(req, res) {
		cashierProc.done()
			.then(function(result) {
				res.json(result);
			})
			.catch(function(error) {
				res.json(error);
			});
	});
}

module.exports = cashierRoutes;